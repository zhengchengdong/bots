package com.bots.http;

public class ExchangeStatistic {

	private long id;
	private long start;
	private long finish;
	private long duringTime;
	private String reqPath;

	public ExchangeStatistic(long id) {
		this.id = id;
	}

	public void calculateDuringTime() {
		duringTime = finish - start;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getFinish() {
		return finish;
	}

	public void setFinish(long finish) {
		this.finish = finish;
	}

	public long getDuringTime() {
		return duringTime;
	}

	public long getId() {
		return id;
	}

	public String getReqPath() {
		return reqPath;
	}

	public void setReqPath(String reqPath) {
		this.reqPath = reqPath;
	}

}
