package com.bots.http;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentResponse;
import org.eclipse.jetty.client.api.Request;

import com.bots.aggr.UserSession;
import com.bots.util.CurrentTimeMillisClock;

public abstract class HttpExchange {

	private static CurrentTimeMillisClock clock = CurrentTimeMillisClock.getInstance();

	private static Map<Long, ExchangeStatistic> statistic = new ConcurrentHashMap<>();

	private static AtomicLong statisticIds = new AtomicLong();

	public static void clearStatistic() {
		statistic.clear();
		statisticIds.set(0);
	}

	public static int countExchanges() {
		return statistic.size();
	}

	public static List<ExchangeStatistic> getStatistics() {
		return new ArrayList<>(statistic.values());
	}

	public void send(HttpClient httpClient, UserSession userSession, Object... parameters) {
		ExchangeStatistic sta = new ExchangeStatistic(statisticIds.incrementAndGet());
		Request request = getRequest(httpClient, userSession, parameters);
		sta.setReqPath(request.getPath());
		ContentResponse response = null;
		try {
			sta.setStart(clock.now());
			response = request.send();
			sta.setFinish(clock.now());
			sta.calculateDuringTime();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			processResponse(response, userSession, parameters);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(
					this.getClass().getSimpleName() + " processResponse error: response=" + response == null ? "null"
							: response.getContentAsString());
		}
		statistic.put(sta.getId(), sta);
		if (userSession != null) {
			List<ExchangeStatistic> httpExchStatList = (List<ExchangeStatistic>) userSession
					.getObject("httpExchStatList");
			if (httpExchStatList == null) {
				httpExchStatList = new ArrayList<>();
				userSession.putObject("httpExchStatList", httpExchStatList);
			}
			httpExchStatList.add(sta);
		}
	}

	protected abstract void processResponse(ContentResponse response, UserSession userSession, Object... parameters);

	protected abstract Request getRequest(HttpClient httpClient, UserSession userSession, Object... parameters);

}
