package com.bots.http;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.util.ssl.SslContextFactory.Client;

public class HTTPClient {
	public static HttpClient httpClient = new HttpClient(new Client(Boolean.TRUE));
	static {
		try {
			httpClient.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static HttpClient newHttpClient() {
		HttpClient newHttpClient = new HttpClient(new Client(Boolean.TRUE));
		try {
			newHttpClient.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newHttpClient;
	}
}
