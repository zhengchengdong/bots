package com.bots.aggr;

import com.bots.http.HTTPClient;

public class CommonHttpStationTemplate implements StationTemplate {

	@Override
	public Station createStation() {
		return new CommonHttpStation(HTTPClient.newHttpClient());
	}

}
