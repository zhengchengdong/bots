package com.bots.aggr;

import org.eclipse.jetty.client.HttpClient;

public class CommonHttpStation implements Station {

	private HttpClient httpClient;

	public CommonHttpStation(HttpClient httpClient) {
		this.httpClient = httpClient;
	}

	public HttpClient getHttpClient() {
		return httpClient;
	}

}
