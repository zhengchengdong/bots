package com.bots.aggr;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UserSession {

	private Map<String, Object> data = new ConcurrentHashMap<>();

	public void putObject(String name, Object obj) {
		data.put(name, obj);
	}

	public Object getObject(String name) {
		return data.get(name);
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

}
