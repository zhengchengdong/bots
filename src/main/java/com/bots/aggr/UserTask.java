package com.bots.aggr;

public class UserTask {
	private long id;// 就是userId
	private Task task;

	public UserTask() {
	}

	public UserTask(long id, Task task) {
		this.id = id;
		this.task = task;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

}
