package com.bots.aggr;

public interface TaskTemplate {

	Task createTask(long userId, String... inputs);

}
