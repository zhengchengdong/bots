package com.bots.aggr;

public class Tasks {
	private int stationsCount;

	private int finishedStationsCount;

	private long start;

	private long finish;

	public void stationFinished(long currTime) {
		finishedStationsCount++;
		if (finishedStationsCount == stationsCount) {
			finish = currTime;
		}
	}

	public int getStationsCount() {
		return stationsCount;
	}

	public void setStationsCount(int stationsCount) {
		this.stationsCount = stationsCount;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getFinish() {
		return finish;
	}

	public void setFinish(long finish) {
		this.finish = finish;
	}

}
