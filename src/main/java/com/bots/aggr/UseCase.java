package com.bots.aggr;

public abstract class UseCase {

	public void start(Task task, Station station) {
		doActions(station);
		task.update(station);
	}

	protected abstract void doActions(Station station);

	public abstract UserSession getUserSession();

	public abstract void setUserSession(UserSession userSession);

}
