package com.bots.aggr;

public interface UserAction {
	void start(Station station);

	void setUserSession(UserSession userSession);

}
