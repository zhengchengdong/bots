package com.bots.aggr;

import java.util.ArrayList;
import java.util.List;

public class Task {
	private List<UseCase> cases = new ArrayList<>();

	private int runningCaseIdx;

	private boolean finished = false;

	public void start(Station station) {
		if (cases.isEmpty()) {
			return;
		}
		cases.get(0).start(this, station);
		runningCaseIdx = 0;
	}

	public void addCase(UseCase useCase) {
		cases.add(useCase);
	}

	public void update(Station station) {
		if (runningCaseIdx < (cases.size() - 1)) {
			runningCaseIdx++;
			cases.get(runningCaseIdx).start(this, station);
		} else {
			finished = true;
		}
	}

	public boolean isFinished() {
		return finished;
	}

	public List<UseCase> getCases() {
		return cases;
	}

	public void setCases(List<UseCase> cases) {
		this.cases = cases;
	}

	public UseCase getCase(int caseIdx) {
		if (caseIdx >= cases.size()) {
			return null;
		}
		return cases.get(caseIdx);
	}

}
