package com.bots.ctrl;

import java.util.HashMap;
import java.util.Map;

public class VO {

	private boolean success;

	private String msg;

	private Object data;

	private VO() {
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public static VO success() {
		VO vo = new VO();
		vo.success = true;
		return vo;
	}

	public static VO success(Object key, Object value) {
		VO vo = new VO();
		vo.success = true;
		Map data = new HashMap();
		data.put(key, value);
		vo.data = data;
		return vo;
	}

	public static VO unsuccess(String msg) {
		VO vo = new VO();
		vo.success = false;
		vo.msg = msg;
		return vo;
	}

	public VO append(Object key, Object value) {
		if (data == null) {
			data = new HashMap();
		}
		((Map) data).put(key, value);
		return this;
	}

	public static VO success(Object data) {
		VO vo = new VO();
		vo.success = true;
		vo.data = data;
		return vo;
	}

}
