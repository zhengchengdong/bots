package com.bots.ctrl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bots.aggr.StationTemplate;
import com.bots.aggr.Task;
import com.bots.aggr.TaskTemplate;
import com.bots.aggr.UseCase;
import com.bots.aggr.UserTask;
import com.bots.http.HttpExchange;
import com.bots.service.TaskService;

@RestController
@RequestMapping("/task")
public class TaskController {

	@Autowired
	private TaskService taskService;

	@RequestMapping("/createtasks")
	@ResponseBody
	public VO createTaskForUsers(long userIdFrom, long usersCount, String taskTpl, String taskInputs) {
		String[] taskInputsStrArray = null;
		if (taskInputs != null) {
			taskInputsStrArray = taskInputs.split("\\|");
		}
		TaskTemplate taskTemplate = null;
		try {
			taskTemplate = (TaskTemplate) Class.forName(taskTpl).newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (long i = 0; i < usersCount; i++) {
			try {
				taskService.createTaskForUser(taskTemplate, userIdFrom + i, taskInputsStrArray);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return VO.success();
	}

	@RequestMapping("/starttasks")
	@ResponseBody
	public VO startTasks(long userIdFrom, long usersCount) {
		for (long i = 0; i < usersCount; i++) {
			taskService.userStartTask(userIdFrom + i);
		}
		return VO.success();
	}

	@RequestMapping("/usersession")
	@ResponseBody
	public VO usersession(long userId, int caseIdx) {
		UseCase useCase = taskService.queryUseCase(userId, caseIdx);
		if (useCase == null) {
			return VO.unsuccess("usersession not found");
		}
		return VO.success(useCase.getUserSession());
	}

	@RequestMapping("/usersessions")
	@ResponseBody
	public VO usersessions(long userIdFrom, long usersCount) {
		List<UserSessionVO> userSessionVOs = new ArrayList<>();
		List<UserTask> userTasks = taskService.queryUserTasks(userIdFrom, usersCount);
		for (UserTask userTask : userTasks) {
			Task task = userTask.getTask();
			for (UseCase useCase : task.getCases()) {
				userSessionVOs.add(new UserSessionVO(userTask, useCase));
			}
		}
		return VO.success(userSessionVOs);
	}

	@RequestMapping("/clear")
	@ResponseBody
	public VO clear() {
		taskService.clear();
		HttpExchange.clearStatistic();
		return VO.success();
	}

	@RequestMapping("/stationsstarttasks")
	@ResponseBody
	public VO startTasks(long userIdFrom, long usersCount, String stationTpl, int stationsCount) {
		StationTemplate stationTemplate = null;
		try {
			stationTemplate = (StationTemplate) Class.forName(stationTpl).newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		taskService.userStartTaskWithStations(userIdFrom, usersCount, stationTemplate, stationsCount);
		return VO.success();
	}

	@RequestMapping("/httpstat")
	@ResponseBody
	public VO httpstat() {
		Map stat = taskService.httpStat();
		return VO.success(stat);
	}

	@RequestMapping("/httpqps")
	@ResponseBody
	public VO httpqps(long i) {
		Map stat = taskService.httpQps(i);
		return VO.success(stat);
	}
}
