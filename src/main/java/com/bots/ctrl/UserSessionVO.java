package com.bots.ctrl;

import java.util.HashMap;
import java.util.Map;

import com.bots.aggr.UseCase;
import com.bots.aggr.UserSession;
import com.bots.aggr.UserTask;

public class UserSessionVO {

	private long userId;

	private String useCase;

	private Map<String, Object> data = new HashMap<>();

	public UserSessionVO() {
	}

	public UserSessionVO(UserTask userTask, UseCase useCase) {
		userId = userTask.getId();
		this.useCase = useCase.getClass().getSimpleName();
		UserSession userSession = useCase.getUserSession();
		if (userSession != null) {
			data.putAll(userSession.getData());
		}
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUseCase() {
		return useCase;
	}

	public void setUseCase(String useCase) {
		this.useCase = useCase;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

}
