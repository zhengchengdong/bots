package com.bots.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import com.bots.aggr.Station;
import com.bots.aggr.StationTemplate;
import com.bots.aggr.TaskTemplate;
import com.bots.aggr.Tasks;
import com.bots.aggr.UseCase;
import com.bots.aggr.UserTask;
import com.bots.http.ExchangeStatistic;
import com.bots.http.HttpExchange;

import arp.process.Process;
import arp.repository.CommonMemRepository;
import arp.repository.SingleEntityRepository;

@Component
@Configuration
@EnableScheduling
public class TaskService {

	private ExecutorService executorService = Executors.newCachedThreadPool();

	private CommonMemRepository<UserTask> userTaskRepository;

	private SingleEntityRepository<Tasks> tasksRepository;

	public TaskService() {
		userTaskRepository = new CommonMemRepository<>(UserTask.class);
		tasksRepository = new SingleEntityRepository<>();
	}

	@Process
	public void createTaskForUser(TaskTemplate taskTemplate, long userId, String... taskInputs) throws Exception {
		UserTask userTask = new UserTask(userId, taskTemplate.createTask(userId, taskInputs));
		userTaskRepository.save(userTask);
	}

	public void userStartTask(long userId) {
		executorService.submit(() -> {
			startTask(userId, null);
		});
	}

	@Process
	public void startTask(long userId, Station station) {
		UserTask userTask = userTaskRepository.findByIdForUpdate(userId);
		if (userTask == null) {
			return;
		}
		userTask.getTask().start(station);
	}

	@Process
	public UseCase queryUseCase(long userId, int caseIdx) {
		UserTask userTask = userTaskRepository.findById(userId);
		if (userTask == null) {
			return null;
		}
		return userTask.getTask().getCase(caseIdx);
	}

	public void clear() {
		userTaskRepository.deleteEntities(userTaskRepository.idSet());
		tasksRepository.put(null);
	}

	public void userStartTaskWithStations(long userIdFrom, long usersCount, StationTemplate stationTepmlate,
			int stationsCount) {
		if (stationsCount > usersCount) {
			stationsCount = (int) usersCount;
		}
		Queue<Long>[] stationUserIds = new Queue[stationsCount];
		for (int i = 0; i < stationsCount; i++) {
			stationUserIds[i] = new LinkedList<Long>();
		}
		for (long i = 0; i < usersCount; i++) {
			int stationIdx = (int) (i % stationsCount);
			stationUserIds[stationIdx].add(userIdFrom + i);
		}

		Station[] stations = new Station[stationsCount];
		for (int i = 0; i < stationsCount; i++) {
			stations[i] = stationTepmlate.createStation();
		}

		recordStartTasks(stationsCount);
		for (int i = 0; i < stationUserIds.length; i++) {
			Queue<Long> userIds = stationUserIds[i];
			Station station = stations[i];
			executorService.submit(() -> {
				for (Long userId : userIds) {
					startTask(userId, station);
					while (!checkTaskFinished(userId)) {
						try {
							Thread.sleep(100);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				updateTasksByStationFinished();
			});
		}
	}

	@Process
	public void updateTasksByStationFinished() {
		Tasks tasks = tasksRepository.getForUpdate();
		tasks.stationFinished(System.currentTimeMillis());
	}

	@Process
	public void recordStartTasks(int stationsCount) {
		Tasks tasks = new Tasks();
		tasks.setStationsCount(stationsCount);
		tasks.setStart(System.currentTimeMillis());
		tasksRepository.put(tasks);
	}

	@Process
	public boolean checkTaskFinished(long userId) {
		UserTask userTask = userTaskRepository.findById(userId);
		if (userTask == null) {
			return true;
		}
		return userTask.getTask().isFinished();
	}

	@Process
	public Map httpStat() {
		Map stat = new HashMap();
		Tasks tasks = tasksRepository.get();
		long startTime = tasks.getStart();
		long statTime = System.currentTimeMillis();
		if (tasks.getFinish() > 0) {
			statTime = tasks.getFinish();
		}
		List<ExchangeStatistic> exchangeStatistics = HttpExchange.getStatistics();
		long httpExchangesCount = exchangeStatistics.size();
		double s = (statTime - startTime) / 1000D;
		double qpsd = httpExchangesCount / s;

		long avgReqRespTime = 0;
		for (ExchangeStatistic exchangeStatistic : exchangeStatistics) {
			avgReqRespTime += exchangeStatistic.getDuringTime();
		}
		avgReqRespTime = avgReqRespTime / httpExchangesCount;
		stat.put("startTime", startTime);
		stat.put("statTime", statTime);
		stat.put("httpExchangesCount", httpExchangesCount);
		stat.put("QPS", (int) qpsd);
		stat.put("avgReqRespTime", avgReqRespTime);
		return stat;
	}

	@Process
	public List<UserTask> queryUserTasks(long userIdFrom, long usersCount) {
		List<UserTask> list = new ArrayList<>();
		for (long i = 0; i < usersCount; i++) {
			UserTask userTask = userTaskRepository.findById(userIdFrom + i);
			if (userTask != null) {
				list.add(userTask);
			}
		}
		return list;
	}

	@Process
	public Map httpQps(long interval) {
		Map stat = new HashMap();
		Tasks tasks = tasksRepository.get();
		long startTime = tasks.getStart();
		long statTime = System.currentTimeMillis();
		if (tasks.getFinish() > 0) {
			statTime = tasks.getFinish();
		}
		long httpExchangesCount = HttpExchange.countExchanges();
		long time = statTime - startTime;
		double s = time / 1000D;
		double qpsd = httpExchangesCount / s;

		int qpsSamplingCount = (int) (time / interval);
		if (time % interval > 0) {
			qpsSamplingCount++;
		}
		int[] exCountArray = new int[qpsSamplingCount];
		for (ExchangeStatistic exchangeStatistic : HttpExchange.getStatistics()) {
			long finish = exchangeStatistic.getFinish();
			if (finish == 0) {
				continue;
			}
			int idx = (int) ((finish - startTime) / interval);
			exCountArray[idx]++;
		}
		long[] qpsArray = new long[qpsSamplingCount];
		for (int i = 0; i < qpsSamplingCount; i++) {
			long statInterval = interval;
			if (i == (qpsSamplingCount - 1)) {
				statInterval = (statTime - (startTime + i * statInterval));
			}
			qpsArray[i] = (long) (exCountArray[i] / (statInterval / 1000D));
		}

		stat.put("startTime", startTime);
		stat.put("statTime", statTime);
		stat.put("httpExchangesCount", httpExchangesCount);
		Map allQps = new HashMap();
		allQps.put("QPS", (int) qpsd);
		allQps.put("interval", interval);
		allQps.put("qpsArray", qpsArray);
		stat.put("allQps", allQps);

		return stat;

	}

}
