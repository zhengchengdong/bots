package test.bots;

import org.junit.Test;

import com.bots.aggr.TaskTemplate;
import com.bots.service.TaskService;

import arp.ARP;

public class TaskTest {

	@Test
	public void test() throws Exception {
		ARP.start("com.bots");

		long userId = 1;
		String loginName = "zcd1";
		TaskService taskService = new TaskService();

		// 给用户1创建了一个任务:
		// 任务顺序执行两个case,代表两个业务。每个case里面用户有一个登录子case和一个提交业务表单action,登录case里面执行一个提交登录action
		taskService.createTaskForUser((TaskTemplate) Class.forName("test.bots.TestTaskTemplate").newInstance(), userId,
				loginName);

		// 启动任务
		taskService.userStartTask(userId);

		while (!taskService.checkTaskFinished(userId)) {
			Thread.sleep(1);
		}

	}

}
