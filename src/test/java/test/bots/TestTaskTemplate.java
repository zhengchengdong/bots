package test.bots;

import com.bots.aggr.Task;
import com.bots.aggr.TaskTemplate;
import com.bots.aggr.UserSession;

public class TestTaskTemplate implements TaskTemplate {

	@Override
	public Task createTask(long userId, String... inputs) {
		Task task = new Task();
		TestBusinessUseCase1 case1 = new TestBusinessUseCase1(inputs[0]);
		case1.setUserSession(new UserSession());
		task.addCase(case1);

		TestBusinessUseCase2 case2 = new TestBusinessUseCase2(inputs[0]);
		case2.setUserSession(new UserSession());
		task.addCase(case2);
		return task;
	}

}
