package test.bots;

import com.bots.aggr.Station;
import com.bots.aggr.UserAction;
import com.bots.aggr.UserSession;

public class TestLoginUserAction implements UserAction {
	private String loginName;
	private UserSession userSession;

	public TestLoginUserAction(String loginName) {
		this.loginName = loginName;
	}

	@Override
	public void start(Station station) {
		userSession.putObject("loginName", loginName);
		System.out.println(loginName + " login done");
	}

	@Override
	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
	}

}
