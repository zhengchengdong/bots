package test.bots;

import com.bots.aggr.Station;
import com.bots.aggr.UseCase;
import com.bots.aggr.UserSession;

public class TestBusinessUseCase2 extends UseCase {

	private UserSession userSession;
	private TestLoginUseCase testLoginUseCase;
	private TestBusinessUserAction2 testBusinessUserAction2;

	public TestBusinessUseCase2(String loginName) {
		this.testLoginUseCase = new TestLoginUseCase(loginName);
		this.testBusinessUserAction2 = new TestBusinessUserAction2();
	}

	@Override
	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
		testLoginUseCase.setUserSession(userSession);
		testBusinessUserAction2.setUserSession(userSession);
	}

	@Override
	public UserSession getUserSession() {
		return userSession;
	}

	@Override
	protected void doActions(Station station) {
		testLoginUseCase.doActions(station);
		// 登录之后等100毫秒模拟实际情况，不必精确知道testLoginUseCase是否结束
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		testBusinessUserAction2.start(station);
	}

}
