package test.bots;

import com.bots.aggr.Station;
import com.bots.aggr.UseCase;
import com.bots.aggr.UserSession;

public class TestLoginUseCase extends UseCase {

	private UserSession userSession;
	private TestLoginUserAction testLoginUserAction;

	public TestLoginUseCase(String loginName) {
		this.testLoginUserAction = new TestLoginUserAction(loginName);
	}

	@Override
	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
		testLoginUserAction.setUserSession(userSession);
	}

	@Override
	public UserSession getUserSession() {
		return userSession;
	}

	@Override
	protected void doActions(Station station) {
		testLoginUserAction.start(station);
	}

}
