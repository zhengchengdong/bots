package test.bots;

import com.bots.aggr.Station;
import com.bots.aggr.UserAction;
import com.bots.aggr.UserSession;

public class TestBusinessUserAction1 implements UserAction {

	private UserSession userSession;

	@Override
	public void start(Station station) {
		System.out.println(userSession.getObject("loginName") + " do some business1");
	}

	@Override
	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
	}

}
